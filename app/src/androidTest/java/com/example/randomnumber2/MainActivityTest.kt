package com.example.randomnumber2

import junit.framework.Assert.assertTrue
import org.junit.Test

class MainActivityTest {

    var min: Int = 0;
    var max: Int = 10;

    @Test
    fun checkMinMax(){
        assertTrue(min < max)
    }

    @Test
    fun generateNumber() {
        val randomInteger = (min..max).random()
        assertTrue(randomInteger in (min + 1) until max)
    }
}