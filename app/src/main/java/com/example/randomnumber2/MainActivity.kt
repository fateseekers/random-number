package com.example.randomnumber2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var max: String
    lateinit var min: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val seekBarMin =  findViewById<SeekBar>(R.id.seekBarMin);
        val seekBarMax =  findViewById<SeekBar>(R.id.seekBarMax);

        seekBarMin.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                Toast.makeText(applicationContext, "Минимальное значение = " + seekBar.progress.toString(), Toast.LENGTH_LONG).show()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                min = progress.toString()
            }
        })
        seekBarMax.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                Toast.makeText(applicationContext, "Максимально значение = " + seekBar.progress.toString(), Toast.LENGTH_LONG).show()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                max = progress.toString()
            }
        })
    }

    private fun getMin(): Int {
        return Integer.parseInt(min);
    }

    private fun getMax(): Int {
        return Integer.parseInt(max);
    }

    fun generateNumber(view: View) {
        if(this.getMin() !== this.getMax()) {
            val randomInteger = (this.getMin()..this.getMax()).random()
            Toast.makeText(this, "Ваше число - $randomInteger", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(this, "Значения должны быть разными", Toast.LENGTH_LONG).show()

        }
    }
}
